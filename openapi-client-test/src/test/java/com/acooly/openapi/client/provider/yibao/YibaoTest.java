package com.acooly.openapi.client.provider.yibao;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.yibao.YibaoApiService;
import com.acooly.module.openapi.client.provider.yibao.message.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhike 2018/6/25 16:21
 */
@SpringBootApplication
@BootApp(sysName = "yibaoTest")
public class YibaoTest extends NoWebTestBase {

    @Autowired
    private YibaoApiService yibaoApiService;


    /**
     * 有短验绑卡请求
     */
    @Test
    public void smsBindCardApply() {
        YibaoSmsBindCardApplyRequest request = new YibaoSmsBindCardApplyRequest();
        request.setOrderNo(Ids.oid());
        request.setIdentityId(Ids.oid());
        request.setIdentityType("USER_ID");
        request.setCardNo("6225365271562822");
        request.setIdCardNo("500221198810192313");
        request.setRealName("志客");
        request.setMobileNo("18696725229");
        request.setAdviceSmsType("MESSAGE");
        YibaoSmsBindCardApplyResponse response = yibaoApiService.smsBindCardApply(request);
        System.out.println("有短验绑卡请求响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试充值订单查询
     */
    @Test
    public void rechargeOrderQuery() {
        YibaoRechargeOrderQueryRequest request = new YibaoRechargeOrderQueryRequest();
        request.setOrderNo(Ids.oid());
        request.setBankOrderNo(Ids.oid());
        YibaoRechargeOrderQueryResponse response = yibaoApiService.rechargeOrderQuery(request);
        System.out.println("充值订单查询响应报文："+ JSON.toJSONString(response));
    }

    /**
     * 测试提现订单查询
     */
    @Test
    public void withdrawOrderQuery() {
        YibaoWithdrawOrderQueryRequest request = new YibaoWithdrawOrderQueryRequest();
        request.setOrderNo(Ids.oid());
        request.setBankOrderNo(Ids.oid());
        YibaoWithdrawOrderQueryResponse response = yibaoApiService.withdrawOrderQuery(request);
        System.out.println("提现订单查询响应报文："+ JSON.toJSONString(response));
    }
}
