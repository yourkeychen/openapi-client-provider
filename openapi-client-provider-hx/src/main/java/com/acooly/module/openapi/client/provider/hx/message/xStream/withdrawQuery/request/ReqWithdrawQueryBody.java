package com.acooly.module.openapi.client.provider.hx.message.xStream.withdrawQuery.request;


import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("body")
public class ReqWithdrawQueryBody {

    /**
     *同 3.3.2 相应报文体中的
     *字段“BatchBillno
     */
    @XStreamAlias("BatchNo")
    private String batchNo;

    /**
     *的字段“MerBillNo” 不输时则查询整
     *个批次
     */
    @XStreamAlias("MerBillNo")
    private String merBillNo;

    /**
     *格式： 年月日(yyyyMMdd)
     *同同 3.2.2 请求报文体中
     *的字段“Date”中的前 8 位字符
     *非必输
     */
    @XStreamAlias("Date")
    private String date;

}
