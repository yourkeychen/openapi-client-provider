/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.fuiou;

import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.api.message.MessageMeta;
import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouResponse;
import com.acooly.module.openapi.client.provider.fuiou.enums.FuiouServiceEnum;
import com.acooly.module.openapi.client.provider.fuiou.message.*;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author zhangpu
 */
@Service
public class FuiouMessageFactory implements MessageFactory {
    public static Map<String, MessageMeta> metas = Maps.newHashMap();

    static {
        /** 网银充值 */
        metas.put(FuiouServiceEnum.Deposit.getKey(),
                // 依次 同步响应，异步通知，同步通知
                new MessageMeta(null, FuiouEbankDepositNotify.class, FuiouEbankDepositResponse.class));
        /** 提现 */
        metas.put(FuiouServiceEnum.Withdraw.getKey(), new MessageMeta(null, FuiouWithdrawNotify.class, FuiouWithdrawResponse.class));
        /** app充值 */
        metas.put(FuiouServiceEnum.AppQuickDeposit.getKey(), new MessageMeta(null, AppQuickDepositNotify.class, AppQuickDepositResponse.class));
        /** app提现 */
        metas.put(FuiouServiceEnum.AppQuickWithdraw.getKey(), new MessageMeta(null, AppQuickWithdrawNotify.class, AppQuickWithdrawResponse.class));
        /** 冻结转冻结 */
        metas.put(FuiouServiceEnum.TransferBuAndFreeze2Freeze.getKey(), new MessageMeta(TransferBuAndFreeze2FreezeResponse.class));
        /** 解冻 */
        metas.put(FuiouServiceEnum.UnFreeze.getKey(), new MessageMeta(UnFreezeResponse.class));
        /** 冻结 */
        metas.put(FuiouServiceEnum.Freeze.getKey(), new MessageMeta(FreezeResponse.class));
        /** 划拨冻结 */
        metas.put(FuiouServiceEnum.TransferBuAndFreeze.getKey(), new MessageMeta(TransferBuAndFreezeResponse.class));
        /** 划拨 */
        metas.put(FuiouServiceEnum.TransferBu.getKey(), new MessageMeta(TransferBuResponse.class));
        /** 转账 */
        metas.put(FuiouServiceEnum.TransferBmu.getKey(), new MessageMeta(TransferBmuResponse.class));
        /** 余额查询 */
        metas.put(FuiouServiceEnum.Balance.getKey(), new MessageMeta(BalanceActionResponse.class));
    }


    @Override
    public ApiMessage getRequest(String serviceName) {
        return null;
    }

    @Override
    public ApiMessage getResponse(String serviceName) {
        if (metas.get(serviceName) == null || metas.get(serviceName).getResponse() == null) {
            return newInstance(FuiouResponse.class);
        } else {
            return newInstance(metas.get(serviceName).getResponse());
        }
    }

    @Override
    public ApiMessage getNotify(String serviceName) {
        return newInstance(metas.get(serviceName).getAsyncNotify());
    }

    @Override
    public ApiMessage getReturn(String serviceName) {
        Class<? extends ApiMessage> clazz = metas.get(serviceName).getSyncNotify();
        if (clazz == null) {
            clazz = metas.get(serviceName).getResponse();
        }
        return newInstance(clazz);
    }

    public <T> T newInstance(Class<T> clazz) {
        try {
            return (T) clazz.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException("InstantiationException:" + clazz);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("IllegalAccessException:" + clazz);
        }
    }
}
