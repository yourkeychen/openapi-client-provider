/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.message;

import com.acooly.module.openapi.client.provider.fuiou.domain.FuiouRequest;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 企业法人注册 请求报文
 * 
 * @author zhangpu
 */
public class FuiouArtifRegRequest extends FuiouRequest {

	/** 企业名称 */
	@NotEmpty
	@Size(min = 1, max = 30)
	@FuiouAlias("cust_nm")
	private String custNm;

	/** 法人姓名 */
	@NotEmpty
	@Size(min = 1, max = 1)
	@FuiouAlias(value = "artif_nm")
	private String artifNm;

	@NotEmpty
	@Size(min = 1, max = 20)
	@FuiouAlias("certif_id")
	private String certifId;

	@NotEmpty
	@Size(min = 11, max = 11)
	@FuiouAlias("mobile_no")
	private String mobileNo;

	@Size(max = 60)
	@FuiouAlias("email")
	private String email;

	@NotEmpty
	@Size(min = 4, max = 4)
	@FuiouAlias("city_id")
	private String cityId;

	/** 提现账户开户银行 */
	@NotEmpty
	@Size(min = 4, max = 4)
	@FuiouAlias("parent_bank_id")
	private String parentBankId;

	/** 提现账户支行名称 */
	@Size(max = 250)
	@FuiouAlias("bank_nm")
	private String bankNm;

	/** 提现银行卡号 */
	@NotEmpty
	@Size(min = 10, max = 30)
	@FuiouAlias
	private String capAcntNo;

	/** 提现密码，不填默认为手机号后6位 MD5加密 */
	@Size(min = 32, max = 32)
	@FuiouAlias
	private String password;

	@Size(min = 32, max = 32)
	@FuiouAlias
	private String lpassword;

	@Size(max = 60)
	@FuiouAlias
	private String rem;

	@NotEmpty
	@FuiouAlias(sign = false)
	private String signature;

	public String getCustNm() {
		return custNm;
	}

	public void setCustNm(String custNm) {
		this.custNm = custNm;
	}

	public String getArtifNm() {
		return artifNm;
	}

	public void setArtifNm(String artifNm) {
		this.artifNm = artifNm;
	}

	public String getCertifId() {
		return certifId;
	}

	public void setCertifId(String certifId) {
		this.certifId = certifId;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getParentBankId() {
		return parentBankId;
	}

	public void setParentBankId(String parentBankId) {
		this.parentBankId = parentBankId;
	}

	public String getBankNm() {
		return bankNm;
	}

	public void setBankNm(String bankNm) {
		this.bankNm = bankNm;
	}

	public String getCapAcntNo() {
		return capAcntNo;
	}

	public void setCapAcntNo(String capAcntNo) {
		this.capAcntNo = capAcntNo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLpassword() {
		return lpassword;
	}

	public void setLpassword(String lpassword) {
		this.lpassword = lpassword;
	}

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

}
