package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoResponse;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/6/26 13:56
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDCARD_CONFIRM,type = ApiMessageType.Response)
public class YibaoSmsBindCardConfirmResponse extends YibaoResponse {

    /**
     * 银行流水号
     */
    @YibaoAlias(value = "yborderid")
    private String bankOrderNo;

    /**
     * 银行编码
     */
    @YibaoAlias(value = "bankcode")
    private String bankCode;

    /**
     * 卡号前六位
     */
    @YibaoAlias(value = "cardtop")
    private String cardTop;

    /**
     * 卡号后四位
     */
    @YibaoAlias(value = "cardlast")
    private String cardLast;

    /**
     * 订单状态
     * BIND_SUCCESS：绑卡成功
     * BIND_FAIL：绑卡失败
     * BIND_ERROR：绑卡异常(可重试)
     * TO_VALIDATE：待短验
     * TIME_OUT：超时失败
     * FAIL：系统异常
     * （FAIL 是非终态是异常状态，出现此状态建议查询）
     */
    @YibaoAlias(value = "status")
    private String status;
}
