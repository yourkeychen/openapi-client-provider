package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.core.utils.Dates;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_BIND_MSG,type = ApiMessageType.Request)
@XStreamAlias("REQUEST")
public class FuyouBindMsgRequest extends FuyouRequest{

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /** 交易请求日期 20180417 */
    @XStreamAlias("TRADEDATE")
    @NotBlank
    @Size(max = 8)
    private String tradeDate = Dates.format(new Date(),"yyyyMMdd");

    /**
     * 商户流水号，保持唯一
     */
    @XStreamAlias("MCHNTSSN")
    @NotBlank
    @Size(max = 30)
    private String orderNo;

    /**
     * 银行卡账户名称
     */
    @XStreamAlias("ACCOUNT")
    @NotBlank
    @Size(max = 30)
    private String reaNname;

    /**
     * 银行卡号
     */
    @XStreamAlias("CARDNO")
    @NotBlank
    @Size(max = 20)
    private String bankCardNo;

    /**
     * 证件类型0：身份证
     */
    @XStreamAlias("IDTYPE")
    @NotBlank
    @Size(max = 2)
    private String idCardType = "0";


    /**
     * 证件号码
     */
    @XStreamAlias("IDCARD")
    @NotBlank
    @Size(max = 30)
    private String idCardNo;

    /**
     * 银行预留手
     *身银行预留手机号
     */
    @XStreamAlias("MOBILENO")
    @NotBlank
    @Size(max = 11)
    private String mobileNo;


    @Override
    public String getSignStr() {
        StringBuilder signStr = new StringBuilder();
        signStr.append(getVersion()).append("|");
        signStr.append(getOrderNo()).append("|");
        signStr.append(getPartner()).append("|");
        signStr.append(getUserId()).append("|");
        signStr.append(getReaNname()).append("|");
        signStr.append(getBankCardNo()).append("|");
        signStr.append(getIdCardType()).append("|");
        signStr.append(getIdCardNo()).append("|");
        signStr.append(getMobileNo()).append("|");
        return signStr.toString();
    }
}
