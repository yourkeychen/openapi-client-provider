/**
 * create by zhangpu
 * date:2015年3月24日
 */
package com.acooly.module.openapi.client.provider.fuyou.enums;

/**
 * @author zhangpu
 *
 */
public enum FuyouSignTypeEnums {
	MD5("md5", "MD5签名"),

	RSA("rsa","RSA签名");


	private String key;
	private String val;

	private FuyouSignTypeEnums(String key, String val) {
		this.key = key;
		this.val = val;
	}

	public String getKey() {
		return key;
	}

	public String getVal() {
		return val;
	}

}
