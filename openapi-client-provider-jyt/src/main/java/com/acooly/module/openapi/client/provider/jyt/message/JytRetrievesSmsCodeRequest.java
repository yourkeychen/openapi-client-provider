package com.acooly.module.openapi.client.provider.jyt.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.jyt.domain.JytApiMsgInfo;
import com.acooly.module.openapi.client.provider.jyt.domain.JytRequest;
import com.acooly.module.openapi.client.provider.jyt.enums.JytServiceEnum;
import com.acooly.module.openapi.client.provider.jyt.message.dto.JytRetrievesSmsCodeRequestBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/8 16:23
 */
@Getter
@Setter
@JytApiMsgInfo(service = JytServiceEnum.RETRIEVES_SMS_CODE,type = ApiMessageType.Request)
@XStreamAlias("message")
public class JytRetrievesSmsCodeRequest extends JytRequest {

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private JytRetrievesSmsCodeRequestBody retrievesSmsCodeRequestBody;

    @Override
    public void doCheck() {
        Validators.assertJSR303(getHeaderRequest());
        Validators.assertJSR303(getRetrievesSmsCodeRequestBody());
    }
}
