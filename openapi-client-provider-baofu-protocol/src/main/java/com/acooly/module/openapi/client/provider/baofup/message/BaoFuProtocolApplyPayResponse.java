package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/13 14:56
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_APPLY_PAY
        ,type = ApiMessageType.Response)
public class BaoFuProtocolApplyPayResponse extends BaoFuPResponse {

    /**
     * 预支付成功后返回
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @Size(max = 126)
    @BaoFuPAlias(value = "unique_code",isDecode = true)
    private String uniqueCode;
}
