package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.*;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * @author zhike 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankMerchantQueryResponseBody extends WsbankCommonMerchantInfo implements Serializable  {

    /**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;
    
	
	@XStreamOmitField
	private WsbankCommonMerchantDetail merchantDetailObj;
	
	@XStreamOmitField
	private List<WsbankCommonMerchantFeeRate> FeeParamListObj;
	
	@XStreamOmitField
	private WsbankCommonMerchantClearCard  merchantClearCardObj;
	
	@XStreamOmitField
	private List<WsbankSiteInfo> wsbankSiteInfoObj;
	
	@XStreamOmitField
    private List<WsWechatChannel> wechatChannelListObj;
	
}
