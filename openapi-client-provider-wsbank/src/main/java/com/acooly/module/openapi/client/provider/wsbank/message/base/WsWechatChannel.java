package com.acooly.module.openapi.client.provider.wsbank.message.base;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

/**
 * @author weili 2018/5/22 18:14
 */
@Getter
@Setter
public class WsWechatChannel implements Serializable {
	
	
	//标识进驻的微信渠道号、进驻结果、微信子商户号信息、进驻失败原因
	private static final long serialVersionUID = 6349060248047361507L;
	
	//ChannelId,Status(0处理中，1成功，2失败),WechatMerchId,FailReason
	private String channelId;


	private String status;

	private String wechatMerchId;
	
	private String failReason;

	 @Override
	public String toString() {
	        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
