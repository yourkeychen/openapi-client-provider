package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("body")
public class WsbankMerchantUnifiedRgisterResponseBody implements Serializable {

	private static final long serialVersionUID = -88986219669638671L;

	/**
     * 返回码组件。当ResultStatus=S时才有后续的参数返回。
     */
    @XStreamAlias("RespInfo")
    private WsbankResponseInfo responseInfo;
    
	/**
	 * 外部商户号
	 */
	@XStreamAlias("OutMerchantId")
	private String outMerchantId;
	
	/**
	 * 申请单号
	 */
	@XStreamAlias("OrderNo")
	private String orderNo;
	
	/**
	 * 外部交易号
	 */
	@XStreamAlias("OutTradeNo")
	private String outTradeNo;

}
