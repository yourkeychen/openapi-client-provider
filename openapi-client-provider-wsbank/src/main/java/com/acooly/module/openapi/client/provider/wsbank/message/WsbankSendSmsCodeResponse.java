package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankResponse;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankSendSmsCodeResponseInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/23 9:53
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.SEND_SMS_CODE,type = ApiMessageType.Response)
public class WsbankSendSmsCodeResponse extends WsbankResponse {

    /**
     * 响应报文信息
     */
    @XStreamAlias("response")
    private WsbankSendSmsCodeResponseInfo responseInfo;
}
