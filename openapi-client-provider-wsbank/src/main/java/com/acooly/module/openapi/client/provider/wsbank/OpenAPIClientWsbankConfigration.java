package com.acooly.module.openapi.client.provider.wsbank;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.notify.WsbankApiServiceClientServlet;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;

import static com.acooly.module.openapi.client.provider.wsbank.OpenAPIClientWsbankProperties.PREFIX;

@EnableConfigurationProperties({OpenAPIClientWsbankProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientWsbankConfigration {

    @Autowired
    private OpenAPIClientWsbankProperties openAPIClientJytProperties;

    @Bean("wsbankHttpTransport")
    public Transport wsbankHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientJytProperties.getGatewayUrl());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientJytProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientJytProperties.getReadTimeout()));
        httpTransport.setContentType("application/xml");
        return httpTransport;
    }

    /**
     * 金运通SDK-servlet注册
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean getWsbankApiSDKServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        WsbankApiServiceClientServlet apiServiceClientServlet = new WsbankApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "wsbankNotifyHandlerDispatcher");
        List<String> urlMappings = Lists.newArrayList();
        //网关异步通知地址
        urlMappings.add("/gateway/notify/wsbankNotify/" + WsbankServiceEnum.PRE_PAY_NOTICE.getCode());//访问，可以添加多个
        urlMappings.add("/gateway/notify/wsbankNotify/" + WsbankServiceEnum.ONLINE_PAY_NOTICE.getCode());//访问，可以添加多个
        urlMappings.add("/gateway/notify/wsbankNotify/" + WsbankServiceEnum.VOSTRO_NOTIFY.getCode());//访问，可以添加多个
        urlMappings.add("/gateway/notify/wsbankNotify/" + WsbankServiceEnum.ORDERSHARE_NOTIFY.getCode());//访问，可以添加多个
        urlMappings.add("/gateway/notify/wsbankNotify/" + WsbankServiceEnum.WITHDRAW_NOTIFY.getCode());//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(2);
        return bean;
    }

//    @Bean("wsbankSftpFileHandler")
//    public JytSftpFileHandler wsbankSftpFileHandler() {
//        JytSftpFileHandler wsbankSftpFileHandler = new JytSftpFileHandler();
//        wsbankSftpFileHandler.setHost(openAPIClientJytProperties.getCheckfile().getHost());
//        wsbankSftpFileHandler.setPort(openAPIClientJytProperties.getCheckfile().getPort());
//        wsbankSftpFileHandler.setUsername(openAPIClientJytProperties.getCheckfile().getUsername());
//        wsbankSftpFileHandler.setPassword(openAPIClientJytProperties.getCheckfile().getPassword());
//        wsbankSftpFileHandler.setServerRoot(openAPIClientJytProperties.getCheckfile().getServerRoot());
//        wsbankSftpFileHandler.setLocalRoot(openAPIClientJytProperties.getCheckfile().getLocalRoot());
//        return wsbankSftpFileHandler;
//    }
}
