package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankOrderShareRequestBody implements Serializable {

	private static final long serialVersionUID = 6082687254084531204L;

	/**
	 * 合作方机构号
	 */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 收款方商户号
     */
    @Size(max = 32)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;

    /**
     * 关联网商订单号
     */
    @Size(max = 32)
    @XStreamAlias("RelateOrderNo")
    @NotBlank
    private String relateOrderNo;

    /**
     * 外部订单分账请求流水号
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     * 订单金额
     */
    @XStreamAlias("TotalAmount")
    private String totalAmount;

    /**
     * 币种
     */
    @Size(max = 3)
    @XStreamAlias("Currency")
    @NotBlank
    private String currency = "CNY";

    /**
     * 付款方资金明细记录
     */
    @Size(max = 512)
    @XStreamAlias("PayerFundDetails")
    private String payerFundDetails;

    /**
     * 收款方资金明细记录
     */
    @Size(max = 512)
    @XStreamAlias("PayeeFundDetails")
    private String payeeFundDetails;

    /**
     * 扩展信息
     */
    @Size(max = 256)
    @XStreamAlias("ExtInfo")
    private String extInfo;

    /**
     * 备注
     */
    @Size(max = 256)
    @XStreamAlias("Memo")
    private String memo;

}
