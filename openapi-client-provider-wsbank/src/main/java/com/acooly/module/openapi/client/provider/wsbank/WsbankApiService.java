/**
 * create by zhike
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.wsbank;

import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.*;
import com.acooly.module.openapi.client.provider.wsbank.message.base.*;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.*;
import com.acooly.module.openapi.client.provider.wsbank.utils.WsBase64Util;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zhike
 */
@Service
@Slf4j
public class WsbankApiService {

    @Resource(name = "wsbankApiServiceClient")
    private WsbankApiServiceClient apiServiceClient;

    @Autowired
    private OpenAPIClientWsbankProperties openAPIClientJytProperties;


    /**
     * 短息发送
     * 合作方在商户进驻、开通银行账户、更换绑定银行卡、更换手机号、账户或余利宝提现等流程中，基于风险控制的需要，需要客户触发网商短信验证码校验的机制。合作方可通过调用本接口触发短信验证码，并在其他交易中上送此验证码。
     * @return
     */
    public WsbankSendSmsCodeResponse sendSmsCode(WsbankSendSmsCodeRequest request) {
        WsbankSendSmsCodeRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.SEND_SMS_CODE.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankSendSmsCodeRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.SEND_SMS_CODE.getCode());
        WsbankSendSmsCodeResponse response = (WsbankSendSmsCodeResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 图片上传
     * 该接口为合作方提供上传商户入驻过程中所需要的图片的能力。注意：该接口以POST方式提交表单，格式为multipart/form-data，并非以报文的形式提交。提交成功后，网商银行将返回图片文件路径。合作方须在商户入驻接口上将图片文件路径一并上送。
     * 图片大小限制在10M以内。 图片上传请求有并发数量限制，须大量请求请提前报备。
     * @return
     */
    public WsbankUploadImgResponse uploadImg(WsbankUploadImgRequest request) {
        request.setAppId(openAPIClientJytProperties.getAppid());
        request.setFunction(WsbankServiceEnum.UPLOAD_IMG.getKey());
        request.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        request.setGatewayUrl(openAPIClientJytProperties.getUploadUrl());
        request.setService(WsbankServiceEnum.UPLOAD_IMG.getCode());
        WsbankUploadImgResponse response = (WsbankUploadImgResponse)apiServiceClient.executeUploadImg(request);
        return response;
    }
    
    /**
     * 商户统一进件接口
     * @return
     */
    public WsbankMerchantUnifiedRgisterResponse merchantUnifiedRgister(WsbankMerchantUnifiedRgisterRequest request) {
        WsbankMerchantUnifiedRgisterRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.MERCHANT_UNIFIED_RGISTER.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankMerchantUnifiedRgisterRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestBody.setAlipaySource(openAPIClientJytProperties.getAlipaySource());
        requestBody.setMerchantDetail(WsBase64Util.encodeStr(JSON.toJSONString(requestBody.getWsbankCommonMerchantDetail())));
        requestBody.setSiteInfo(WsBase64Util.encodeStr(requestBody.getSiteInfo()));
        requestBody.setFeeParamList(WsBase64Util.encodeStr(requestBody.getFeeParamList()));
        requestBody.setBankCardParam(WsBase64Util.encodeStr(JSON.toJSONString(requestBody.getWsbankCommonMerchantClearCard())));
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.MERCHANT_UNIFIED_RGISTER.getCode());
        WsbankMerchantUnifiedRgisterResponse response = (WsbankMerchantUnifiedRgisterResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 订单查询
     * 本接口为所有支付订单（主扫、被扫）提供查询功能，合作方可以通过该接口主动查询订单（退款单交易除外）交易结果，明确交易结果后进行下一步的业务逻辑。
     * @return
     */
    public WsbankPayQueryResponse payQuery(WsbankPayQueryRequest request) {
        WsbankPayQueryRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.TRADE_ORDER_QUERY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankPayQueryRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.TRADE_ORDER_QUERY.getCode());
        WsbankPayQueryResponse response = (WsbankPayQueryResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 动态订单扫码支付接口
     * @return
     */
    public WsbankBkmerchanttradedynamicOrderResponse bkmerchanttradedynamicOrder(WsbankBkmerchanttradedynamicOrderRequest request) {
        request.setGatewayUrl(openAPIClientJytProperties.getSyncGatewayUrl());
    	WsbankBkmerchanttradedynamicOrderRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BKMERCHANTTRADE_DYNAMICORDER.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkmerchanttradedynamicOrderRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BKMERCHANTTRADE_DYNAMICORDER.getCode());
        WsbankBkmerchanttradedynamicOrderResponse response = (WsbankBkmerchanttradedynamicOrderResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 支付宝APP/WAP支付接口
     * @return
     */
    public WsbankAlipayResponse alipayRequest(WsbankAlipayRequest request) {
        request.setGatewayUrl(openAPIClientJytProperties.getSyncGatewayUrl());
    	WsbankAlipayRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.ALIPAY_REQUEST.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankAlipayRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.ALIPAY_REQUEST.getCode());
        WsbankAlipayResponse response = (WsbankAlipayResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 订单关闭接口
     * @return
     */
    public WsbankBkmerchanttradePayCloseResponse bkmerchanttradePayClose(WsbankBkmerchanttradePayCloseRequest request) {
        request.setGatewayUrl(openAPIClientJytProperties.getSyncGatewayUrl());
    	WsbankBkmerchanttradePayCloseRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BKMERCHANTTRADE_PAYCLOSE.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkmerchanttradePayCloseRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BKMERCHANTTRADE_PAYCLOSE.getCode());
        WsbankBkmerchanttradePayCloseResponse response = (WsbankBkmerchanttradePayCloseResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 订单关闭接口
     * @return
     */
    public WsbankBkmerchanttradePayCancelResponse bkmerchanttradePayCancel(WsbankBkmerchanttradePayCancelRequest request) {
        request.setGatewayUrl(openAPIClientJytProperties.getSyncGatewayUrl());
    	WsbankBkmerchanttradePayCancelRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BKMERCHANTTRADE_PAYCANCEL.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkmerchanttradePayCancelRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BKMERCHANTTRADE_PAYCANCEL.getCode());
        WsbankBkmerchanttradePayCancelResponse response = (WsbankBkmerchanttradePayCancelResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 退款接口
     * @return
     */
    public WsbankBkmerchanttradeRefundResponse bkmerchanttradeRefund(WsbankBkmerchanttradeRefundRequest request) {
    	WsbankBkmerchanttradeRefundRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BKMERCHANTTRADE_REFUND.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkmerchanttradeRefundRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BKMERCHANTTRADE_REFUND.getCode());
        WsbankBkmerchanttradeRefundResponse response = (WsbankBkmerchanttradeRefundResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 退款查询
     * @return
     */
    public WsbankBkmerchanttradeRefundQueryResponse bkmerchanttradeRefundQuery(WsbankBkmerchanttradeRefundQueryRequest request) {
    	WsbankBkmerchanttradeRefundQueryRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BKMERCHANTTRADE_REFUND_QUERY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkmerchanttradeRefundQueryRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BKMERCHANTTRADE_REFUND_QUERY.getCode());
        WsbankBkmerchanttradeRefundQueryResponse response = (WsbankBkmerchanttradeRefundQueryResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 商户入驻结果查询
     * 该接口可查询商户入驻申请结果。当合作方请求商户入驻后，网商银行将在一定时间内（5分钟到24小时不等）。合作方可通过该接口回查商户入驻申请结果。
     */
    public WsbankRegisterQueryResponse registerQuery(WsbankRegisterQueryRequest request) {
        WsbankRegisterQueryRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.REGISTER_QUERY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankRegisterQueryRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.REGISTER_QUERY.getCode());
        WsbankRegisterQueryResponse response = (WsbankRegisterQueryResponse)apiServiceClient.execute(request);
        if(null!=response && null !=response.getResponseInfo()){
        	WsbankRegisterQueryResponseBody body = response.getResponseInfo().getResponseBody();
       	    body.setWechatChannelListObj(WsBase64Util.decodeList(body.getWechatChannelList(),WsWechatChannel.class));
       }
        return response;
    }


    /**
     * 微信子商户支付配置接
     * 规则说明：
     * 1.需要配置支付授权目录时，单独调用接口，单独传递path字段
     * 2.需要绑定支付appid和推荐关注公众号时，调用接口，同时传递subappid和subscribeappid字段。多次调用可以配置多个支付appid和推荐关注公众号关系。
     * 3.该接口只支持新增配置，不支持修改
     * @param request
     * @return
     */
    public WsbankAddMerchantConfigResponse addMerchantConfig(WsbankAddMerchantConfigRequest request) {
        WsbankAddMerchantConfigRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.ADD_MERCHANT_CONFIG.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankAddMerchantConfigRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.ADD_MERCHANT_CONFIG.getCode());
        WsbankAddMerchantConfigResponse response = (WsbankAddMerchantConfigResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 合作方可通过该接口查询本机构名下进驻的商户信息。
     * @param request
     * @return
     */
	public WsbankMerchantQueryResponse  merchantQuery(WsbankMerchantQueryRequest request){
    	  WsbankMerchantQueryRequestInfo requestInfo = request.getRequestInfo();
          WsbankHeadRequest headRequest = new WsbankHeadRequest();
          headRequest.setAppid(openAPIClientJytProperties.getAppid());
          headRequest.setFunction(WsbankServiceEnum.MERCHANT_QUERY.getKey());
          requestInfo.setHeadRequest(headRequest);
          WsbankMerchantQueryRequestBody requestBody = requestInfo.getRequestBody();
          requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
          requestInfo.setRequestBody(requestBody);
          request.setService(WsbankServiceEnum.MERCHANT_QUERY.getCode());
          WsbankMerchantQueryResponse response = (WsbankMerchantQueryResponse)apiServiceClient.execute(request);
          if(null!=response && null !=response.getResponseInfo()){
        	  	WsbankMerchantQueryResponseBody body = response.getResponseInfo().getResponseBody();
				body.setMerchantDetailObj(WsBase64Util.decode(body.getMerchantDetail(), WsbankCommonMerchantDetail.class));
				body.setFeeParamListObj(WsBase64Util.decodeList(body.getFeeParamList(),WsbankCommonMerchantFeeRate.class));
				body.setMerchantClearCardObj(WsBase64Util.decode(body.getBankCardParam(),WsbankCommonMerchantClearCard.class));
				body.setWsbankSiteInfoObj(WsBase64Util.decodeList(body.getSiteInfo(),WsbankSiteInfo.class));
				 body.setWechatChannelListObj(WsBase64Util.decodeList(body.getWechatChannelList(),WsWechatChannel.class));
          }
          return response;
    }

    /**
     * 	统一进件商户信息修
     * @param request
     * @return
     */
    public WsbankMerchantUpdateResponse merchantUpdate(WsbankMerchantUpdateRequest request){
  	    WsbankMerchantUpdateRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.MERCHANT_UPDATE.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankMerchantUpdateRequestBody  requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        if(requestBody.getMerchantDetailObj()!=null){
        	  requestBody.setMerchantDetail(WsBase64Util.encode(requestBody.getMerchantDetailObj()));
        }
        if(requestBody.getBankCardParam()!=null){
            requestBody.setBankCardParam(WsBase64Util.encode(requestBody.getBankCardParam()));
        }
        requestBody.setSiteInfo(WsBase64Util.encodeStr(requestBody.getSiteInfo()));
//        requestBody.setFeeParamList(WsBase64Util.encode(requestBody.getFeeParamList()));
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.MERCHANT_UPDATE.getCode());
        WsbankMerchantUpdateResponse response = (WsbankMerchantUpdateResponse)apiServiceClient.execute(request);
        return response;
  }
    
    /**
     * 商户关闭接口
     * @param request
     * @return
     */
    public WsbankMerchantFreezeResponse  merchantFreeze(WsbankMerchantFreezeRequest request){
    	WsbankMerchantFreezeRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.MERCHANT_FREEZE.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankMerchantFreezeRequestBody  requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.MERCHANT_FREEZE.getCode());
        WsbankMerchantFreezeResponse response = (WsbankMerchantFreezeResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 商户开启接口
     * @param request
     * @return
     */
    public WsbankMerchantUnFreezeResponse merchantUnFreeze(WsbankMerchantUnFreezeRequest request){
    	WsbankMerchantUnFreezeRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.MERCHANT_UNFREEZE.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankMerchantUnFreezeRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.MERCHANT_UNFREEZE.getCode());
        WsbankMerchantUnFreezeResponse response = (WsbankMerchantUnFreezeResponse)apiServiceClient.execute(request);
        return response;
    }

    
    /**
     * 来帐汇入订单创建
     * @param request
     * @return
     */
    public WsBankBkCloudFundsVostroCreateOrderResponse vostroCreateOrder(WsBankBkCloudFundsVostroCreateOrderRequest request) {
    	WsBankBkCloudFundsVostroCreateOrderRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.VOSTRO_CREATE_ORDER.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsBankBkCloudFundsVostroCreateOrderRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.VOSTRO_CREATE_ORDER.getCode());
        WsBankBkCloudFundsVostroCreateOrderResponse response = (WsBankBkCloudFundsVostroCreateOrderResponse)apiServiceClient.execute(request);
        return response;
    }  

    /**
     * 移动刷卡支付（被扫）
     * @param request
     * @return
     */
    public WsbankBkmerchanttradePayResponse bkmerchanttradePay(WsbankBkmerchanttradePayRequest request){
        request.setGatewayUrl(openAPIClientJytProperties.getSyncGatewayUrl());
    	WsbankBkmerchanttradePayRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BKMERCHANTTRADE_PAY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkmerchanttradePayRequestBody  requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BKMERCHANTTRADE_PAY.getCode());
        WsbankBkmerchanttradePayResponse response = (WsbankBkmerchanttradePayResponse)apiServiceClient.execute(request);
        return response;
  }
    /**
     * 移动刷卡支付（主扫）
     * @param request
     * @return
     */
    public WsbankBkmerchanttradePrePayResponse bkmerchanttradePrePay(WsbankBkmerchanttradePrePayRequest request){
        request.setGatewayUrl(openAPIClientJytProperties.getSyncGatewayUrl());
    	WsbankBkmerchanttradePrePayRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BKMERCHANTTRADE_PREPAY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkmerchanttradePrePayRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BKMERCHANTTRADE_PREPAY.getCode());
        WsbankBkmerchanttradePrePayResponse response = (WsbankBkmerchanttradePrePayResponse)apiServiceClient.execute(request);
        return response;
  }
    
    /**
     * 单笔来帐汇入订单查询
     * @param request
     * @return
     */
    public WsbankBkCloudFundsVostroQueryOrderResponse vostroQueryOrder(WsbankBkCloudFundsVostroQueryOrderRequest request) {
    	WsbankBkCloudFundsVostroQueryOrderRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.VOSTRO_QUERY_ORDER.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBkCloudFundsVostroQueryOrderRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.VOSTRO_QUERY_ORDER.getCode());
        WsbankBkCloudFundsVostroQueryOrderResponse response = 
        		(WsbankBkCloudFundsVostroQueryOrderResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 通过此接口为进驻成功的商户，开通余额支付功能，余额支付功能需要使用短信动态码，所以开通余额支付的时候，
     * 会使用商户入驻的时候清算卡加上绑定的负责人手机号（PrincipalMobile）进行四要素验证（合规要求，支付的时候短信动态码发送到绑定手机上），
     * 四要素验证通过之后方可开通余额支付功能，如果四要素验证失败，需要通过商户进驻修改接口，修改清算卡或者关联手机号。
     * 然后重新发起开通商户余额支付能力。
     * @param request
     * @return
     */
    public WsbankOpenPayResponse openPay(WsbankOpenPayRequest request) {
        WsbankOpenPayRequestInfo requestInfo = request.getWsbankOpenPayRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.OPEN_PAY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankOpenPayRequestBody requestBody = requestInfo.getWsbankOpenPayRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setWsbankOpenPayRequestBody(requestBody);
        request.setService(WsbankServiceEnum.OPEN_PAY.getCode());
        WsbankOpenPayResponse response = (WsbankOpenPayResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 买家使用余额支付接口，从买家可用余额扣除，转入到卖家的可用余额。先决条件是用买家绑定清算的关联手机号收到的动态验证码。
     * @param request
     * @return
     */
    public WsbankCreateBalancePayResponse createBalancePay(WsbankCreateBalancePayRequest request) {
        WsbankCreateBalancePayRequestInfo requestInfo = request.getWsbankCreateBalancePayRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BALANCE_PAY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankCreateBalancePayRequestBody requestBody = requestInfo.getWsbankCreateBalancePayRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setWsbankCreateBalancePayRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BALANCE_PAY.getCode());
        WsbankCreateBalancePayResponse response = (WsbankCreateBalancePayResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 余额支付确认
     * @param request
     * @return
     */
    public WsbankBalancePayConfirmResponse balancePayConfirm(WsbankBalancePayConfirmRequest request) {
        WsbankBalancePayConfirmRequestInfo requestInfo = request.getWsbankBalancePayConfirmRequestInfo();

        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BALANCE_PAY_CONFIRM.getKey());
        requestInfo.setHeadRequest(headRequest);

        WsbankBalancePayConfirmRequestBody requestBody = requestInfo.getWsbankBalancePayConfirmRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setWsbankBalancePayConfirmRequestBody(requestBody);
        request.setService(WsbankServiceEnum.BALANCE_PAY_CONFIRM.getCode());

        WsbankBalancePayConfirmResponse response = (WsbankBalancePayConfirmResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 余额支付查询
     * @param request
     * @return
     */
    public WsbankBalancePayQueryResponse balancePayQuery(WsbankBalancePayQueryRequest request) {
        WsbankBalancePayQueryRequestInfo requestInfo = request.getWsbankBalancePayQueryRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.BALANCE_PAY_QUERY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankBalancePayQueryRequestBody body = requestInfo.getWsbankBalancePayQueryRequestBody();
        body.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setWsbankBalancePayQueryRequestBody(body);
        request.setWsbankBalancePayQueryRequestInfo(requestInfo);
        request.setService(WsbankServiceEnum.BALANCE_PAY_QUERY.getCode());
        WsbankBalancePayQueryResponse response = (WsbankBalancePayQueryResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 分账接口
     * @param request
     * @return
     */
    public WsbankOrderShareResponse orderShare(WsbankOrderShareRequest request) {
        WsbankOrderShareRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.ORDER_SHARE.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankOrderShareRequestBody body = requestInfo.getWsbankOrderShareRequestBody();
        body.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        if(StringUtils.isNotBlank(body.getPayeeFundDetails())){
        	body.setPayeeFundDetails(WsBase64Util.encodeStr(body.getPayeeFundDetails()));
        }
        if(StringUtils.isNotBlank(body.getPayerFundDetails())){
        	body.setPayerFundDetails(WsBase64Util.encodeStr(body.getPayerFundDetails()));
        }
        requestInfo.setWsbankOrderShareRequestBody(body);
        request.setService(WsbankServiceEnum.ORDER_SHARE.getCode());
        WsbankOrderShareResponse response = (WsbankOrderShareResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 通过此查询接口，针对某笔分账交易进行单笔查询。
     * @param request
     * @return
     */
    public WsbankOrderShareQueryResponse orderShareQuery(WsbankOrderShareQueryRequest request) {
        WsbankOrderShareQueryRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.ORDER_SHARE_QUERY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankOrderShareQueryRequestBody body = requestInfo.getBody();
        body.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setBody(body);
        request.setService(WsbankServiceEnum.ORDER_SHARE_QUERY.getCode());
        WsbankOrderShareQueryResponse response = (WsbankOrderShareQueryResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     * 通过此接口，请求单笔提现到商户绑定的清算卡。
     * @param request
     * @return
     */
    public WsbankWithdrawApplyResponse withdrawApply(WsbankWithdrawApplyRequest request) {
        WsbankWithdrawApplyRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.WITHDRAW_APPLY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankWithdrawApplyRequestBody body = requestInfo.getWsbankWithdrawApplyRequestBody();
        body.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setWsbankWithdrawApplyRequestBody(body);
        request.setService(WsbankServiceEnum.WITHDRAW_APPLY.getCode());

        WsbankWithdrawApplyResponse  response = (WsbankWithdrawApplyResponse)apiServiceClient.execute(request);
        return response;
    }
    
    /**
     * 通过此确认接口，请求单笔提现到商户绑定的清算卡。
     * @param request
     * @return
     */
    public WsbankWithdrawConfirmResponse withdrawConfirm(WsbankWithdrawConfirmRequest request) {
    	WsbankWithdrawConfirmRequestInfo requestInfo = request.getRequestInfo();
    	WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.WITHDRAW_CONFIRM.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankWithdrawConfirmRequestBody requestBody = requestInfo.getRequestBody();
        requestBody.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setRequestBody(requestBody);
        request.setService(WsbankServiceEnum.WITHDRAW_CONFIRM.getCode());
        WsbankWithdrawConfirmResponse response = (WsbankWithdrawConfirmResponse)apiServiceClient.execute(request);
        return response;
    }

    /**
     *
     * @param request
     * @return
     */
    public WsbankWithdrawQueryResponse withdrawQuery(WsbankWithdrawQueryRequest request) {
        WsbankWithdrawQueryRequestInfo requestInfo = request.getRequestInfo();
        WsbankHeadRequest headRequest = new WsbankHeadRequest();
        headRequest.setAppid(openAPIClientJytProperties.getAppid());
        headRequest.setFunction(WsbankServiceEnum.WITHDRAW_APPLY_QUERY.getKey());
        requestInfo.setHeadRequest(headRequest);
        WsbankWithdrawQueryRequestBody body = requestInfo.getBody();
        body.setIsvOrgId(openAPIClientJytProperties.getPartnerId());
        requestInfo.setBody(body);
        request.setService(WsbankServiceEnum.WITHDRAW_APPLY_QUERY.getCode());
        WsbankWithdrawQueryResponse response = (WsbankWithdrawQueryResponse)apiServiceClient.execute(request);
        return response;
    }
}
