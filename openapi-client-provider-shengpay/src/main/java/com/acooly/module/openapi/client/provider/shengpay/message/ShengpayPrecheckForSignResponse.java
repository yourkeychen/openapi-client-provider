package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayResponse;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/5/17 18:55
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.PRECHECK_FOR_SIGN,type = ApiMessageType.Response)
public class ShengpayPrecheckForSignResponse extends ShengpayResponse {
}
