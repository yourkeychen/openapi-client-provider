package com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author fufeng 2018/1/26 15:26.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("BODY")
public class QdReqBody {

    @XStreamAlias("QUERY_TRANS")
    private QdReqQueryTrans queryTrans;
    @XStreamAlias("QUERY_DETAILS")
    private QdReqQueryDetails queryDetails;
}
