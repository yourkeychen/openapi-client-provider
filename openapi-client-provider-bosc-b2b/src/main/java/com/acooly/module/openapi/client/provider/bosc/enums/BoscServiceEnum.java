package com.acooly.module.openapi.client.provider.bosc.enums;

public enum BoscServiceEnum {

	openAcct("openAcct", "开户"),

	activateAcct("activateAcct", "激活账户"),

	queryVirAcctBalance("queryVirAcctBalance", "查询虚账户余额"),

	queryAcctStatus("queryAcctStatus", "查询企业户激活状态"),

	virAcctIn("virAcctIn", "虚账户单笔代发"),

	upBindCard("upBindCard", "换绑功能");

	private String key;
	private String val;

	private BoscServiceEnum(String key, String val) {
		this.key = key;
		this.val = val;
	}

	public String getKey() {
		return key;
	}

	public String getVal() {
		return val;
	}

}
