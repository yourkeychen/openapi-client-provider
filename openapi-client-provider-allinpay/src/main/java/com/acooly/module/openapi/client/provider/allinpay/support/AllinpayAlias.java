/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhangpu
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface AllinpayAlias {

	/**
	 * 是否签名
	 * @return
	 */
	boolean sign() default true;

	/**
	 * 别名
	 * @return
	 */
	String value();

	/**
	 * 是否放入请求报文
	 * @return
	 */
	boolean request() default true;

}
