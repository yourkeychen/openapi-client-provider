/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月30日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标准字段的序列化顺序
 * 
 * @author zhike
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ItemOrder {
	/**
	 * 顺序
	 * 
	 * @return
	 */
	public int value() default 0;
}
