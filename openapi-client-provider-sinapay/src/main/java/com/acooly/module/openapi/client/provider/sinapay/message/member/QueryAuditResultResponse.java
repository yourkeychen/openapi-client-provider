package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:30
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_AUDIT_RESULT, type = ApiMessageType.Response)
public class QueryAuditResultResponse extends SinapayResponse {

    /**
     *审核结果
     * 审核结果，详见附录“审核状态”
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "audit_result")
    private String auditResult;

    /**
     *审核结果建议
     * 当审核失败时，查看该字段
     */
    @Size(max = 200)
    @ApiItem(value = "audit_mgs")
    private String auditMgs;
}
