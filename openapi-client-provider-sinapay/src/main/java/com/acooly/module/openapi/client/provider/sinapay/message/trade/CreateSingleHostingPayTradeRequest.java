/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayAccountType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayOutTradeCode;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.DebtChangeDetail;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.TradePayItem;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author zhike
 */
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_SINGLE_HOSTING_PAY_TRADE, type = ApiMessageType.Request)
public class CreateSingleHostingPayTradeRequest extends SinapayRequest {

	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "out_trade_no")
	private String outTradeNo = Ids.oid();

	/**
	 * 交易码
	 *
	 * 商户网站代收交易业务码，见附录
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "out_trade_code")
	private String outTradeCode = SinapayOutTradeCode.INVEST_OUT.code();

	/**
	 * 收款人标识
	 *
	 * 商户系统用户ID、钱包系统会员ID
	 */
	@NotEmpty
	@Size(max = 50)
	@ApiItem(value = "payee_identity_id")
	private String payeeIdentityId;

	/**
	 * 收款人标识类型
	 *
	 * ID的类型，参考“标志类型”
	 */
	@NotEmpty
	@Size(max = 16)
	@ApiItem(value = "payee_identity_type")
	private String payeeIdentityType = "UID";

	/**
	 * 收款人账户类型
	 *
	 * 账户类型（基本户、保证金户）。默认基本户，见附录
	 */
	@Size(max = 16)
	@ApiItem(value = "account_type")
	private String accountType = SinapayAccountType.SAVING_POT.code();

	/**
	 * 金额
	 *
	 * 金额
	 */
	@MoneyConstraint
	@ApiItem(value = "amount")
	private Money amount;

	/**
	 * 分账信息列表
	 *
	 * 收款信息列表，参见收款信息，参数间用“^”分隔，各条目之间用“|”分隔，备注信息不要包含特殊分隔符
	 */
	@ApiItem(value = "split_list")
	private List<TradePayItem> splitList = Lists.newArrayList();

	/**
	 * 摘要
	 *
	 * 交易内容摘要
	 */
	@NotEmpty
	@Size(max = 64)
	@ApiItem(value = "summary")
	private String summary;

	/**
	 * 用户IP/运营商IP
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "user_ip")
	private String userIp;

	/**
	 * 商户标的号
	 *
	 * 对应“标的录入”接口中的标的号，用来关联此笔代收和标的
	 */
	@Size(max = 64)
	@ApiItem(value = "goods_id")
	private String goodsId;

	/**
	 * 交易关联号
	 *
	 * 商户交易关联号，用于代收代付交易关联
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "trade_related_no")
	private String tradeRelatedNo;

	/**
	 * 债权变动明细列表
	 */
	@ApiItem(value = "creditor_info_list")
	private List<DebtChangeDetail> creditorInfoList = Lists.newArrayList();
	
	public CreateSingleHostingPayTradeRequest() {
		super();
	}

	/**
	 * @param payeeIdentityId
	 * @param amount
	 * @param summary
	 */
	public CreateSingleHostingPayTradeRequest(String payeeIdentityId, Money amount, String summary, String userIp) {
		this();
		this.payeeIdentityId = payeeIdentityId;
		this.amount = amount;
		this.summary = summary;
		this.userIp = userIp;
	}
}
