package com.acooly.module.openapi.client.provider.yuejb;

import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBNotify;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBRequest;
import com.acooly.module.openapi.client.provider.yuejb.domain.YueJBResponse;
import com.acooly.module.openapi.client.provider.yuejb.message.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 月结宝
 * Created by ouwen@yiji.com} on 2017/11/6.
 */
@Service
public class YueJBApiService {

    @Resource(name = "yueJBApiServiceClient")
    private ApiServiceClient<YueJBRequest, YueJBResponse, YueJBNotify, YueJBNotify> apiServiceClient;
    /**
     * 额度冻结
     * @param request
     */
    public YueJBFreezeResponse freeze(YueJBFreezeRequest request){
        request.setService(YueJBConstants.FREEZE);
        return (YueJBFreezeResponse) apiServiceClient.execute(request);
    }

    /**
     * 放款
     * @param request
     * @return
     */
    public YueJBLoanResponse loan(YueJBLoanRequest request){
        request.setService(YueJBConstants.LOAN);
        return (YueJBLoanResponse) apiServiceClient.execute(request);
    }

    /**
     * 异步通知
     * @param notity
     * @return
     */
    public YueJBLoanNotify notifyMessage(Map<String,String> notity){
        return (YueJBLoanNotify) apiServiceClient.notice(notity,YueJBConstants.LOAN);
    }
}
