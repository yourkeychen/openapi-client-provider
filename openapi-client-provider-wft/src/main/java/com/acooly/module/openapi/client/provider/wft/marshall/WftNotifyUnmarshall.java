/**
 * create by zhangpu date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.wft.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.wft.domain.WftNotify;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author zhangpu
 */
@Service
@Slf4j
public class WftNotifyUnmarshall extends WftMarshallSupport
        implements ApiUnmarshal<WftNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(WftNotifyUnmarshall.class);

    @Resource(name = "wftMessageFactory")
    private MessageFactory messageFactory;

    @SuppressWarnings("unchecked")
    @Override
    public WftNotify unmarshal(Map<String, String> notifyMessage, String serviceName) {
        try {
            log.info("异步通知报文{}", notifyMessage);
            // 验签
            String status = notifyMessage.get("status");
            if (Strings.equals(status, "0")) {
                String partnerId = notifyMessage.get("mch_id");
                SortedMap<String, String> sort = new TreeMap<String, String>(notifyMessage);
                verifySign(sort, partnerId);
            } else {
                log.info("响应状态status={}通讯失败", status);
                throw new ApiClientException("通讯失败");
            }
            return doUnmarshall(notifyMessage, serviceName);
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }
    }

    protected WftNotify doUnmarshall(Map<String, String> message, String serviceName) {
        WftNotify notify =
                (WftNotify) messageFactory.getNotify(serviceName);
        Set<Field> fields = Reflections.getFields(notify.getClass());
        String key = null;
        for (Field field : fields) {
            WftAlias wftAlias = field.getAnnotation(WftAlias.class);
            if (wftAlias != null && Strings.isNotBlank(wftAlias.value())) {
                key = wftAlias.value();
            } else {
                key = field.getName();
            }
            Reflections.invokeSetter(notify, field.getName(), message.get(key));
        }
        return notify;
    }

    public void verifySign(SortedMap<String, String> params, String partnerId) {
        try {
            String signature = params.get("sign");
            doVerifySign(params, signature, partnerId);
            log.info("验签成功");
        } catch (Exception e) {
            log.info("验签失败，响应报文：{}", params);
            throw new ApiClientException("响应报文验签失败");
        }
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }
}
