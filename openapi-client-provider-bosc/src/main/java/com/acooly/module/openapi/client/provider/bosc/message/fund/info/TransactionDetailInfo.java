/*
 *
 * www.cutebear.com Inc.
 * Copyright (c) 2017  All Rights Reserved
 */

package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by liubin@prosysoft.com on 2017/10/16.
 */
public class TransactionDetailInfo {
	
	/**
	 * 标的号
	 */
	@Size(max=50)
	private String projectNo;
	/**
	 * 同【预处理业务类型】
	 */
	@NotNull
	private BoscBizTypeEnum confirmTradeType;
	/**
	 * 交易确认请求流水号
	 */
	@NotEmpty
	@Size(max=50)
	private String requestNo;
	/**
	 * 平台佣金
	 */
	@MoneyConstraint
	private Money commission;
	/**
	 * 交易发起时间
	 */
	@NotNull
	private Date createTime;
	/**
	 * SUCCESS 表示成功，FAIL 表示失败，INIT 表示初始化，ERROR 表示异常，ACCEPT 表示已受理， PROCESSING 表示处理中
	 */
	@NotEmpty
	@Size(max=50)
	private String status;
	/**
	 * 交易完成时间
	 */
	private Date transactionTime;
	/**
	 * 错误码
	 */
	private String errorCode;
	/**
	 * 错误描述
	 */
	private String errorMessage;
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public BoscBizTypeEnum getConfirmTradeType () {
		return confirmTradeType;
	}
	
	public void setConfirmTradeType (BoscBizTypeEnum confirmTradeType) {
		this.confirmTradeType = confirmTradeType;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public Money getCommission () {
		return commission;
	}
	
	public void setCommission (Money commission) {
		this.commission = commission;
	}
	
	public Date getCreateTime () {
		return createTime;
	}
	
	public void setCreateTime (Date createTime) {
		this.createTime = createTime;
	}
	
	public String getStatus () {
		return status;
	}
	
	public void setStatus (String status) {
		this.status = status;
	}
	
	public Date getTransactionTime () {
		return transactionTime;
	}
	
	public void setTransactionTime (Date transactionTime) {
		this.transactionTime = transactionTime;
	}
	
	public String getErrorCode () {
		return errorCode;
	}
	
	public void setErrorCode (String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage () {
		return errorMessage;
	}
	
	public void setErrorMessage (String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
