package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.UNFREEZE, type = ApiMessageType.Request)
public class UnfreezeRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 原冻结的请求流水号（通用冻结的解冻不能传入该参数）
	 */
	@Size(max = 50)
	private String originalFreezeRequestNo;
	/**
	 * 解冻金额
	 */
	@MoneyConstraint(nullable = false, min = 1)
	private Money amount;
	/**
	 * 通用冻结金额解冻时需要传入的参数
	 */
	@Size(max = 50)
	private String platformUserNo;
	
	public UnfreezeRequest () {
		setService (BoscServiceNameEnum.UNFREEZE.code ());
	}
	
	public UnfreezeRequest (String requestNo, Money amount) {
		this();
		this.requestNo = requestNo;
		this.amount = amount;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getOriginalFreezeRequestNo () {
		return originalFreezeRequestNo;
	}
	
	public void setOriginalFreezeRequestNo (String originalFreezeRequestNo) {
		this.originalFreezeRequestNo = originalFreezeRequestNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
}