package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscTradeTypeEnum;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
public class BizDetailInfo implements Serializable{
	
	@NotBlank(message = "交易明细订单号不能为空")
	private String requestNo;
	
	@NotNull(message = "交易类型不能为空")
	private BoscTradeTypeEnum tradeType;
	
	/**
	 * 债权出让请求流水号
	 */
	@Max (value = 50)
	private String saleRequestNo;
	
	/**
	 * 标的编号
	 */
	@Max (value = 50)
	private String projectNo;
	
	@NotEmpty
	private List<DetailInfo> details;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public BoscTradeTypeEnum getTradeType () {
		return tradeType;
	}
	
	public void setTradeType (BoscTradeTypeEnum tradeType) {
		this.tradeType = tradeType;
	}
	
	public List<DetailInfo> getDetails () {
		return details;
	}
	
	public void setDetails (List<DetailInfo> details) {
		this.details = details;
	}
	
	public String getSaleRequestNo () {
		return saleRequestNo;
	}
	
	public void setSaleRequestNo (String saleRequestNo) {
		this.saleRequestNo = saleRequestNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
}
