/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-22 11:45 创建
 */
package com.acooly.module.openapi.client.provider.bosc.domain;

import com.acooly.module.openapi.client.provider.bosc.enums.BoscCodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscStatusEnum;

/**
 * @author zhangpu 2017-09-22 11:45
 */
public class BoscResponse extends BoscMessage {

    private String code;

    private BoscStatusEnum status;

    private String errorCode;

    private String errorMessage;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    public BoscStatusEnum getStatus () {
        return status;
    }
    
    public void setStatus (BoscStatusEnum status) {
        this.status = status;
    }
    
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
    public boolean isSuccess(){
        return BoscCodeEnum.SUCCESS.getCode ().equals (getCode ()) && BoscStatusEnum.SUCCESS.equals (status);
    }
}
