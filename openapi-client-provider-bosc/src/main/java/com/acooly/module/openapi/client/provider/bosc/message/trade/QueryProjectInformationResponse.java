package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscProjectStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscProjectTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscRepaymentWayEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_PROJECT_INFORMATION, type = ApiMessageType.Response)
public class QueryProjectInformationResponse extends BoscResponse {
	/**
	 * 借款方平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	/**
	 * 标的金额
	 */
	@MoneyConstraint(min = 1)
	private Money projectAmount;
	/**
	 * 标的名称
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectName;
	/**
	 * 见【标的产品类型】
	 */
	@NotNull
	private BoscProjectTypeEnum projectType;
	/**
	 * 标的期限（单位：天）
	 */
	private int projectPeriod;
	/**
	 * 标的属性（STOCK 为存量标的，NEW 为新增标的）
	 */
	private String projectProperties;
	/**
	 * 年化利率
	 */
	@NotEmpty
	private String annualInterestRate;
	/**
	 * 见【还款方式】
	 */
	@NotNull
	private BoscRepaymentWayEnum repaymentWay;
	/**
	 * 见【标的状态】
	 */
	@NotNull
	private BoscProjectStatusEnum projectStatus;
	/**
	 * 已投标确认金额
	 */
	@MoneyConstraint
	private Money loanAmount;
	/**
	 * 已还款确认本金
	 */
	@MoneyConstraint
	private Money repaymentAmount;
	/**
	 * 已还利息
	 */
	@MoneyConstraint
	private Money income;
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public Money getProjectAmount () {
		return projectAmount;
	}
	
	public void setProjectAmount (Money projectAmount) {
		this.projectAmount = projectAmount;
	}
	
	public String getProjectName () {
		return projectName;
	}
	
	public void setProjectName (String projectName) {
		this.projectName = projectName;
	}
	
	public BoscProjectTypeEnum getProjectType () {
		return projectType;
	}
	
	public void setProjectType (BoscProjectTypeEnum projectType) {
		this.projectType = projectType;
	}
	
	public int getProjectPeriod () {
		return projectPeriod;
	}
	
	public void setProjectPeriod (int projectPeriod) {
		this.projectPeriod = projectPeriod;
	}
	
	public String getProjectProperties () {
		return projectProperties;
	}
	
	public void setProjectProperties (String projectProperties) {
		this.projectProperties = projectProperties;
	}
	
	public String getAnnualInterestRate () {
		return annualInterestRate;
	}
	
	public void setAnnualInterestRate (String annualInterestRate) {
		this.annualInterestRate = annualInterestRate;
	}
	
	public BoscRepaymentWayEnum getRepaymentWay () {
		return repaymentWay;
	}
	
	public void setRepaymentWay (BoscRepaymentWayEnum repaymentWay) {
		this.repaymentWay = repaymentWay;
	}
	
	public BoscProjectStatusEnum getProjectStatus () {
		return projectStatus;
	}
	
	public void setProjectStatus (BoscProjectStatusEnum projectStatus) {
		this.projectStatus = projectStatus;
	}
	
	public Money getLoanAmount () {
		return loanAmount;
	}
	
	public void setLoanAmount (Money loanAmount) {
		this.loanAmount = loanAmount;
	}
	
	public Money getRepaymentAmount () {
		return repaymentAmount;
	}
	
	public void setRepaymentAmount (Money repaymentAmount) {
		this.repaymentAmount = repaymentAmount;
	}
	
	public Money getIncome () {
		return income;
	}
	
	public void setIncome (Money income) {
		this.income = income;
	}
}