package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotify;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscRollbackTypeEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@ApiMsgInfo(service = BoscServiceNameEnum.BACKROLL_RECHARGE, type = ApiMessageType.Notify)
public class BackrollRechargeNotify extends BoscNotify {
	/**
	 * 回退充值请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 回退到账金额（提现实际入账金额）
	 */
	@MoneyConstraint(min = 1)
	private Money rollbackAmount;
	/**
	 * 回退佣金（提现佣金）
	 */
	@MoneyConstraint(nullable = true)
	private Money rollbackCommission;
	/**
	 * 提现请求流水号（提现失败对应的提现请求流水号）
	 */
	@NotEmpty
	@Size(max = 50)
	private String withdrawRequestNo;
	/**
	 * 回退充值完成时间
	 */
	@NotNull
	private Date completedTime;
	
	/**
	 * 资金回充类型：INTERCEPT（表示提现拦截以后系统发起的回充）、REMITFAIL（表示提现出款失败后系统发起的回充）
	 */
	@NotNull
	private BoscRollbackTypeEnum rollbackType;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public Money getRollbackAmount () {
		return rollbackAmount;
	}
	
	public void setRollbackAmount (Money rollbackAmount) {
		this.rollbackAmount = rollbackAmount;
	}
	
	public Money getRollbackCommission () {
		return rollbackCommission;
	}
	
	public void setRollbackCommission (Money rollbackCommission) {
		this.rollbackCommission = rollbackCommission;
	}
	
	public String getWithdrawRequestNo () {
		return withdrawRequestNo;
	}
	
	public void setWithdrawRequestNo (String withdrawRequestNo) {
		this.withdrawRequestNo = withdrawRequestNo;
	}
	
	public Date getCompletedTime () {
		return completedTime;
	}
	
	public void setCompletedTime (Date completedTime) {
		this.completedTime = completedTime;
	}
	
	public BoscRollbackTypeEnum getRollbackType () {
		return rollbackType;
	}
	
	public void setRollbackType (BoscRollbackTypeEnum rollbackType) {
		this.rollbackType = rollbackType;
	}
}