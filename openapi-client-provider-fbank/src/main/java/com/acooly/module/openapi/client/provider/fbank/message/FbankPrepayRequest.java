package com.acooly.module.openapi.client.provider.fbank.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.fbank.domain.FbankRequest;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceEnum;
import com.acooly.module.openapi.client.provider.fbank.enums.FbankServiceTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 16:08
 */
@Getter
@Setter
@FbankApiMsgInfo(service = FbankServiceEnum.PREPAY_API, type = ApiMessageType.Request,serviceType = FbankServiceTypeEnum.payol)
public class FbankPrepayRequest extends FbankRequest {
    /**
     * 订单总金额
     * 必须为正整数（单位为分）
     */
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 商品名称
     * 商户自定义订单标题
     */
    @NotBlank
    @Size(max = 64)
    private String subject;

    /**
     * 订单描述
     * 商户自定义订单描述
     */
    @NotBlank
    @Size(max = 256)
    private String body;

    /**
     * 商户订单号
     * 商户自己平台的订单号（唯一）
     */
    @NotBlank
    @Size(max = 64)
    private String mchntOrderNo;

    /**
     * 支付功能id
     */
    @NotBlank
    @Size(max = 12)
    private String payPowerId;

    /**
     * 订单附加描述
     * 商户下单时所传
     */
    @Size(max = 512)
    private String description;

    /**
     * 接口版本号
     * 此版本传api_1.0
     */
    @NotBlank
    @Size(max = 32)
    private String version = "api_1.0";

    /**
     * 用户真实ip
     * 用户发起请求的ip；
     * 在请求微信h5接口时，必须传真实的用户端IP,详见 ：https://pay.weixin.qq.com/wiki/doc/api/H5_sl.php?chapter=15_5
     * 所传非真实客户端ip，会报：网络环境未能通过安全验证，请稍后再试，详见：
     * https://pay.weixin.qq.com/wiki/doc/api/H5_sl.php?chapter=15_4
     */
    @NotBlank
    @Size(max = 16)
    private String clientIp;

    /**
     * 附加数据
     * 商户下单时所传
     */
    @Size(max = 256)
    private String extra;

    /**
     * 商户分发渠道
     * 商户自定义来标记该笔订单属于的渠道方（支付平台会记录该标记，但不做任何处理）
     */
    @Size(max = 256)
    private String cpChannel;

    /**
     * 币种
     * 默认为RMB(人民币)，目前只支持人民币。
     */
    @Size(max = 3)
    private String currency = "RMB";

    /**
     * 支付码
     * 发起条码支付时的支付码。
     * 当支付功能为微信条码支付、支付宝条码支付时，此参数必填。
     */
    @Size(max = 64)
    private String authCode;

    /**
     * 设备终端号
     * 发起条码支付时此参数必填
     */
    @Size(max = 32)
    private String terminalId;

    /**
     * 京东H5支付返回方式
     * 此字段为： mweb_url时，返回参数中包含 mweb_url为URL，可直接唤起京东支付，其他情况，则返回参数中包含 jdResult，为form表单页面字符串形式；
     */
    private String jdH5ReturnType;

    /**
     * 商户公众号号APPID
     * 商户传入此appid时，则使用此appid;如不传，则使用默认的appid (是否有默认的appid,需要富民运营配置)；此字段是V1.6.2版本增加的，为了支持商户可使用自己的公众号，同时也可使用代理商的公众号；
     */
    @Size(max = 40)
    private String subAppId;

    /**
     * 用户在微信公众号或者微信小程序下的唯一标识
     * 发起微信公众号或微信小程序支付时(在富民支付平台报备成功的微信公众号或小程序)，此参数必填。获取subOpenId请参考：
     *
     * 微信公众号：
     * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1445241432
     *
     * 微信小程序：
     * https://mp.weixin.qq.com/debug/wxadoc/dev/api/api-login.html
     */
    @Size(max = 64)
    private String subOpenId;

    /**
     * 异步通知地址
     * 接收支付平台异步通知回调地址，通知url必须为外网可访问的ur；
     * 当支付方式为微信条码、支付宝条码支付时，不进行异步通知。
     */
    @Size(max = 128)
    private String notifyUrl;

    /**
     * 同步跳转地址
     * 当 payPowerId=3或者 payPowerId=23或者 payPowerId=24时可传此参数。详情参考6.2.3章节。
     */
    @Size(max = 128)
    private String returnUrl;

    /**
     * 场景信息
     * 商户对接微信h5接口时，此参数必填。
     * 1，	WAP网站应用，传值格式：Wap|网站名称|网站地址  例如：
     * Wap|腾讯充值|http://pay.qq.com
     * 2，	IOS移动应用，传值格式：IOS|应用名称|bundle_id  例如：
     *  IOS|王者荣耀|com.tencent.wzryIOS
     * 3，	安卓移动应用，传值格式：Android|应用名称|包名   例如：
     *   Android|王者荣耀|com.tencent.tmgp.sgame
     */
    @Size(max = 512)
    private String sceneInfo;

    /**
     * 用户在京东、支付宝平台唯一id（支付宝固定二维码、京东支付用到）
     * 当且仅当payPowerId=23或者payPowerId=24时（京东支付），即进件京东pc网页支付和京东h5支付时，必须填写（可随机生成）。填写用户id用于绑定用户下次支付，无需登录即可调起支付页面
     *
     * 当payPowerId= 3或payPowerId =17时（支付宝jsapi相关支付），
     * 可选填该字段；如果传值，则代表商户自己获取用户的userid，同时响应  bankTransactionId参数;如果不传，则由富民平台获取userid参数，同时响应 authUrl参数。获取支付宝openId(userid)可参考下面链接：
     * https://doc.open.alipay.com/docs/doc.htm?spm=a219a.7629140.0.0.U57SiP&treeId=193&articleId=106001&docType=1#s3 中的第四步；
     */
    @Size(max = 64)
    private String openId;

    /**
     * 交易订单类型（京东支付用到）
     * 当且仅当payPowerId=23或者payPowerId=24时，即进件京东pc网页支付和京东h5支付时，必须填写。 0.实物 1.虚拟
     */
    @Size(max = 32)
    private String reserved;

    /**
     * 订单有效时长
     * 条码支付不支持,微信（扫码，公众号，app，小程序）有效时长为2小时。例如：2
     */
    @Size(max = 3)
    private String timeoutExpress;
}
